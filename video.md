MSA
====
microservices learning

# Overview

## Video

### Webinar
* [Webinar: Overview and Core Values of Domain-Driven Design - Part 1/5](https://www.youtube.com/watch?v=Z0zpSB85XGs&list=PLcd4r_IW3i9BwFYvdzVfShDTK8AqSvIV-)
* [Webinar: Strategic Design: Domains, Subdomains, Bounded Contexts & Context Maps - Part 2/5](https://www.youtube.com/watch?v=pNTsTVoV2qA&list=PLcd4r_IW3i9BwFYvdzVfShDTK8AqSvIV-&index=2)
* [Webinar: Internal Building Blocks: Aggregates, Entities, Value Objects Part 3/5](https://www.youtube.com/watch?v=RHg53wMflCc&list=PLcd4r_IW3i9BwFYvdzVfShDTK8AqSvIV-&index=5)
* [Webinar: Domain Events & Event Storming Part Four](https://www.youtube.com/watch?v=yWDaBdV-SL8&t=1s)
* [Webinar: Implementation Aspects with Java and Spring (Boot, Data & Cloud) Part Five](https://www.youtube.com/watch?v=DNOLwyfliLE)

### 2013
* [REST-Ful API Design](https://www.youtube.com/watch?v=oG2rotiGr90)

### 2014

### 2015
* [Spring Boot Micro-services, Containers, and Kubernetes - How To](https://www.youtube.com/watch?v=Bcs-inRnLDc)
* [GOTO 2015 • DDD & REST - Domain Driven APIs for the Web • Oliver Gierke](https://www.youtube.com/watch?v=1RgXgZcj5nM)
* [GOTO 2015 • DDD & Microservices: At Last, Some Boundaries! • Eric Evans](https://www.youtube.com/watch?v=yPvef9R3k-M)

### 2016
* [RabbitMQ Introduction](https://www.youtube.com/watch?v=-td8qgCU75E)
* [What I Wish I Had Known Before Scaling Uber to 1000 Services • Matt Ranney](https://www.youtube.com/watch?v=kb-m2fasdDY)


### 2017
* [Developing microservices with aggregates - Chris Richardson](https://www.youtube.com/watch?v=7kX3fs0pWwc)
* [Four Distributed Systems Architectural Patterns by Tim Berglund](https://www.youtube.com/watch?v=tpspO9K28PM)
* [The Many Meanings of Event-Driven Architecture • Martin Fowler](https://www.youtube.com/watch?v=STKCRSUsyP0)
* [10 Tips for failing badly at Microservices by David Schmitz](https://www.youtube.com/watch?v=X0tjziAQfNQ)
* [Mastering Chaos - A Netflix Guide to Microservices](https://www.youtube.com/watch?v=CZ3wIuvmHeM)
* [Eric Evans - Keynote: Tackling Complexity in the Heart of Software](https://www.youtube.com/watch?v=kIKwPNKXaLU)
* [Introduction to Microservices, Docker, and Kubernetes](https://www.youtube.com/watch?v=1xo-0gCVhTU)

### 2018
* [Data Consistency in Microservices Architecture (Grygoriy Gonchar)](https://www.youtube.com/watch?v=CFdPDfXy6Y0)
* [Microservices + Events + Docker = A Perfect Trio](https://www.youtube.com/watch?v=sSm2dRarhPo)
* [OAuth 2.0 and OpenID Connect (in plain English)](https://www.youtube.com/watch?v=996OiexHze0)
* [Data Consistency in Microservice Using Sagas](https://www.youtube.com/watch?v=txlSrGVCK18) 
* [RabbitMQ vs Kafka - Jack Vanlightly x Erlang Solutions webinar](https://www.youtube.com/watch?v=sjDnqrnnYNM)

### 2019
* [The Right Way to DevOps with Terraform and Ansible](https://www.youtube.com/watch?v=AsPIKWF1y_M)
